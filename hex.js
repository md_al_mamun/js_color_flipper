const hex = [0,1,2,3,4,5,6,7,8,9, "A", "B", "C", "D", "E", "F"]

const btn = document.getElementById("btn");
const color = document.querySelector(".color");

btn.addEventListener('click', ChangeColor_HEX );

function ChangeColor_HEX(){    
    let hexColor = "#";

    for(let i = 0; i<6; i++ ){
        let rendomNumber = Math.floor(Math.random() * hex.length);
        hexColor += hex[rendomNumber];
    }

    color.textContent = hexColor;
    document.body.style.backgroundColor = hexColor;
}

